FROM docker.io/akamai/shell:latest

WORKDIR /workdir
RUN chmod 0777 .
# Declare as a volume AFTER creating it, otherwise the chmod does not work
VOLUME /workdir

RUN akamai install --force https://github.com/apiheat/akamai-cli-netstorage

#ARG TURL=https://releases.hashicorp.com/terraform/1.1.2/terraform_1.1.2_linux_amd64.zip
ARG TERRAFORM_VERSION=1.1.5
ARG TERRAFORM_SHA256SUM=30942d5055c7151f051c8ea75481ff1dc95b2c4409dbb50196419c21168d6467
# sha256sum needs two spaces between the checksum and the filename!!!
RUN curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip > terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && echo "${TERRAFORM_SHA256SUM} *terraform_${TERRAFORM_VERSION}_linux_amd64.zip" > terraform_${TERRAFORM_VERSION}_SHA256SUMS \
    && sha256sum -c terraform_${TERRAFORM_VERSION}_SHA256SUMS \
    && unzip -o terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin \
    && rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip terraform_${TERRAFORM_VERSION}_SHA256SUMS

ENTRYPOINT ["/bin/bash", "-lc", "${0} ${1+\"$@\"}"]
