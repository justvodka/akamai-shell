## [1.2.3](https://gitlab.com/justvodka/akamai-shell/compare/v1.2.2...v1.2.3) (2022-02-04)


### Bug Fixes

* **Terraform:** Terraform to 1.1.5 ([542df20](https://gitlab.com/justvodka/akamai-shell/commit/542df20c3369eecba7d3f4a9d61689ac42806a1f))

## [1.2.2](https://gitlab.com/justvodka/akamai-shell/compare/v1.2.1...v1.2.2) (2022-01-07)


### Bug Fixes

* **ci/image:** addedd --all back anx reverted to regular docker repo ([9c61068](https://gitlab.com/justvodka/akamai-shell/commit/9c61068c19e622812c4c26c330b65318e9ed0e39))
* **ci/image:** changed manifest format to v2s2 ([be4de9b](https://gitlab.com/justvodka/akamai-shell/commit/be4de9b8ad7da58b65ca87271b93abb53477e683))
* **image/ci:** does manifest work without the all ([5464c99](https://gitlab.com/justvodka/akamai-shell/commit/5464c99277c24282ecb6c90e099305597cffed14))
* **image/ci:** manifest updates ([5b26ef3](https://gitlab.com/justvodka/akamai-shell/commit/5b26ef322e456d2d6347e4cc7e3e6a556547a99d))
* **image:** try to add manifest to container registry ([eeec01b](https://gitlab.com/justvodka/akamai-shell/commit/eeec01be05bbd47721d34137ac86795a7104e340))

## [1.2.1](https://gitlab.com/justvodka/akamai-shell/compare/v1.2.0...v1.2.1) (2022-01-07)


### Bug Fixes

* **ci/image:** addedd --all back anx reverted to regular docker repo ([9c61068](https://gitlab.com/justvodka/akamai-shell/commit/9c61068c19e622812c4c26c330b65318e9ed0e39))

## [1.2.1-beta.3](https://gitlab.com/justvodka/akamai-shell/compare/v1.2.1-beta.2...v1.2.1-beta.3) (2022-01-07)


### Bug Fixes

* **image/ci:** does manifest work without the all ([5464c99](https://gitlab.com/justvodka/akamai-shell/commit/5464c99277c24282ecb6c90e099305597cffed14))

## [1.2.1-beta.2](https://gitlab.com/justvodka/akamai-shell/compare/v1.2.1-beta.1...v1.2.1-beta.2) (2022-01-07)


### Bug Fixes

* **image/ci:** manifest updates ([5b26ef3](https://gitlab.com/justvodka/akamai-shell/commit/5b26ef322e456d2d6347e4cc7e3e6a556547a99d))
* **image:** try to add manifest to container registry ([eeec01b](https://gitlab.com/justvodka/akamai-shell/commit/eeec01be05bbd47721d34137ac86795a7104e340))
* **terraform:** added terraform version 1.1.3 ([61a9056](https://gitlab.com/justvodka/akamai-shell/commit/61a9056e1cd68bd18985b4894f7fedc65f6e7416))

## [1.2.1-beta.1](https://gitlab.com/justvodka/akamai-shell/compare/v1.2.0...v1.2.1-beta.1) (2022-01-07)


### Bug Fixes

* **terraform:** added terraform version 1.1.3 ([61a9056](https://gitlab.com/justvodka/akamai-shell/commit/61a9056e1cd68bd18985b4894f7fedc65f6e7416))

# [1.2.0](https://gitlab.com/justvodka/akamai-shell/compare/v1.1.0...v1.2.0) (2022-01-07)


### Bug Fixes

* **Dockerfile:** add -o to unzip for alowed overwrite ([012c5a7](https://gitlab.com/justvodka/akamai-shell/commit/012c5a710c6b58cbf8e910f027c54a9229ac9bf9))


### Features

* **image:** terraform 1.1.2 added ([5e8d458](https://gitlab.com/justvodka/akamai-shell/commit/5e8d45885d5b1b0a1653b42f7158144f1bc41824))

# [1.2.0-beta.1](https://gitlab.com/justvodka/akamai-shell/compare/v1.1.0...v1.2.0-beta.1) (2022-01-07)


### Bug Fixes

* **Dockerfile:** add -o to unzip for alowed overwrite ([012c5a7](https://gitlab.com/justvodka/akamai-shell/commit/012c5a710c6b58cbf8e910f027c54a9229ac9bf9))


### Features

* **image:** terraform 1.1.2 added ([5e8d458](https://gitlab.com/justvodka/akamai-shell/commit/5e8d45885d5b1b0a1653b42f7158144f1bc41824))

# [1.1.0](https://gitlab.com/justvodka/akamai-shell/compare/v1.0.5...v1.1.0) (2022-01-07)


### Bug Fixes

* **ci:** curly bracket at the wrong place doesn't work for some reason.. should go back to bed. ([5cb084c](https://gitlab.com/justvodka/akamai-shell/commit/5cb084cab4b6fb8199c4395fe12fd87dc33c0c4c))
* **ci:** forgot to close the if statement ([3f2f636](https://gitlab.com/justvodka/akamai-shell/commit/3f2f636a86860cfdd61e6a66fd38541902879b00))
* **ci:** if you don't add "$" then variables will never expand :facepalm: ([cfcc79d](https://gitlab.com/justvodka/akamai-shell/commit/cfcc79d0058c06a2b93d98239a3838458588f630))
* **ci:** moved release type testing to build stage ([a680079](https://gitlab.com/justvodka/akamai-shell/commit/a68007924dd9941a40c06117d4e1275b52594eb3))
* **ci:** removed typo ([6f130e8](https://gitlab.com/justvodka/akamai-shell/commit/6f130e8e82bb7fb09cdb3693573b67a2ad645591))


### Features

* **image:** add platform ([61a3f68](https://gitlab.com/justvodka/akamai-shell/commit/61a3f68f2d96d54b46079e3c050d0f81885db358))
* **image:** added akamai-cli-netstorage ([a55145f](https://gitlab.com/justvodka/akamai-shell/commit/a55145f3ec678963db2789ebfe5da0e0eb2f0283))
* **image:** added image build ([6c978dc](https://gitlab.com/justvodka/akamai-shell/commit/6c978dc6f3cbeffa79657fc767768f4d9a613c19))

# [1.1.0-alpha.3](https://gitlab.com/justvodka/akamai-shell/compare/v1.1.0-alpha.2...v1.1.0-alpha.3) (2022-01-07)
## [1.0.5](https://gitlab.com/justvodka/akamai-shell/compare/v1.0.4...v1.0.5) (2022-01-06)


### Bug Fixes

* **ci:** curly bracket at the wrong place doesn't work for some reason.. should go back to bed. ([5cb084c](https://gitlab.com/justvodka/akamai-shell/commit/5cb084cab4b6fb8199c4395fe12fd87dc33c0c4c))
* **ci:** if you don't add "$" then variables will never expand :facepalm: ([cfcc79d](https://gitlab.com/justvodka/akamai-shell/commit/cfcc79d0058c06a2b93d98239a3838458588f630))

# [1.1.0-alpha.2](https://gitlab.com/justvodka/akamai-shell/compare/v1.1.0-alpha.1...v1.1.0-alpha.2) (2022-01-07)


### Features

* **image:** add platform ([61a3f68](https://gitlab.com/justvodka/akamai-shell/commit/61a3f68f2d96d54b46079e3c050d0f81885db358))

# [1.1.0-alpha.1](https://gitlab.com/justvodka/akamai-shell/compare/v1.0.5-alpha.2...v1.1.0-alpha.1) (2022-01-07)


### Bug Fixes

* **ci:** forgot to close the if statement ([3f2f636](https://gitlab.com/justvodka/akamai-shell/commit/3f2f636a86860cfdd61e6a66fd38541902879b00))
* **ci:** moved release type testing to build stage ([a680079](https://gitlab.com/justvodka/akamai-shell/commit/a68007924dd9941a40c06117d4e1275b52594eb3))
* **ci:** removed typo ([6f130e8](https://gitlab.com/justvodka/akamai-shell/commit/6f130e8e82bb7fb09cdb3693573b67a2ad645591))


### Features

* **image:** added image build ([6c978dc](https://gitlab.com/justvodka/akamai-shell/commit/6c978dc6f3cbeffa79657fc767768f4d9a613c19))
* **Dockerfile:** for another merge request ([6e24e5f](https://gitlab.com/justvodka/akamai-shell/commit/6e24e5f1c7e9dae9af86ea64df58bd2e816207db))
* **Dockerfile:** to see how alpha versioning works ([062f7ea](https://gitlab.com/justvodka/akamai-shell/commit/062f7ea7c33f43922b96e68249aa34c94f68f6dc))

## [1.0.5-alpha.2](https://gitlab.com/justvodka/akamai-shell/compare/v1.0.5-alpha.1...v1.0.5-alpha.2) (2022-01-06)


### Bug Fixes

* **Dockerfile:** for another merge request ([6e24e5f](https://gitlab.com/justvodka/akamai-shell/commit/6e24e5f1c7e9dae9af86ea64df58bd2e816207db))

## [1.0.5-alpha.1](https://gitlab.com/justvodka/akamai-shell/compare/v1.0.4...v1.0.5-alpha.1) (2022-01-06)


### Bug Fixes

* **Dockerfile:** to see how alpha versioning works ([062f7ea](https://gitlab.com/justvodka/akamai-shell/commit/062f7ea7c33f43922b96e68249aa34c94f68f6dc))

## [1.0.4](https://gitlab.com/justvodka/akamai-shell/compare/v1.0.3...v1.0.4) (2022-01-06)


### Bug Fixes

* **ci:** my bad I need to sleep wrong plugin reference ([32f2383](https://gitlab.com/justvodka/akamai-shell/commit/32f2383d80cd38d58e7172dd0c4b74593bbea760))
* **ci:** why o why doesn't it work? ([9c2d101](https://gitlab.com/justvodka/akamai-shell/commit/9c2d101de9b70adb5bdd6512f982b40abf865050))
